#pragma once

#include <initializer_list>

template<class T>
class Array {
public:
    explicit Array(size_t, const T& = T()) {}

    template<class Iterator>
    Array(Iterator, Iterator) {}

    Array(std::initializer_list<T>) {}

    const T& operator[](size_t) const {
        static T dummy;
        return dummy;
    }
};
