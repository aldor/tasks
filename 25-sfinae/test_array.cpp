#include <vector>
#include <string>

#include <gtest/gtest.h>
#include <array.h>

TEST(Array, SimpleConstructors) {
    Array<int> simple(5);
    ASSERT_EQ(0, simple[2]);
    Array<std::string> str{"aba", "caba"};
    ASSERT_EQ("caba", str[1]);
    Array<std::string> copy(4, "a");
    ASSERT_EQ("a", copy[3]);
}

TEST(Array, Ranges) {
    std::vector<int> v{1, 2, 3};
    Array<int> a(v.begin(), v.end());
    ASSERT_EQ(v[2], a[2]);
    int raw[] = {1, 2, 3};
    Array<int> b(raw, raw + 3);
    ASSERT_EQ(raw[1], b[1]);
}

TEST(Array, ShitHappens) {
    Array<int> a(3, 2);
    ASSERT_EQ(2, a[2]);
    ASSERT_EQ(2, a[0]);
    Array<double> b(4, 2);
    ASSERT_EQ(2.0, b[1]);
    Array<size_t> c(3, 3);
    ASSERT_EQ(3, c[1]);
}

