<!-- $size: B4 -->
<!-- $theme: default -->
<!-- page_number: true -->
<!-- *template: invert -->

# Введение в boost.asio

###### Grigory Demchenko (gridem@yandex-team.ru)

---

## Сетевое программирование

Зачем?

---

## Сетевое программирование

- Межпроцессное взаимодействие
- Удаленное взаимодействие
	- Внутри ДЦ
	- Между ДЦ
	- Геораспределенное

Основано на посылке сообщений между участниками.

---

## Основные протоколы

- TCP - обеспечивает иллюзию передачи непрерывного потока байт
	- следит за дубликатами пакетов
	- перепосылает в случае утери
	- базовая проверка целостности 
- UDP - single shot: отсылка одного сообщения
	- потеря пакетов	

---

## Использование

Каждая ОС использует свой набор примитивов для эффективного взаимодействия с железом:

- Linux: `epoll`
- FreeBSD/MacOSX: `kqueue`
- Windows: IOCP

Как быть?

---

## Библиотека `boost.asio`

- __Кроссплатформенная__: работает на множестве платформ.
- __Эффективная__: использует наиболее эффективную реализацию на каждой из платформ.
- __Простая__: более простое использование абстракций.
- __Гибкая__: подходит для широкого класса задач.

---

## Архитектура

![архитектура](04/overview.png)

---

## Пример синхронного взаимодействия

```cpp
using boost::asio;

// создание экземпляра ioService для взаимодействия с ОС
io_service ioService;
// создание сокета для подключения
ip::tcp::socket socket(ioService);
// подключение к серверу к serverEndpoint
socket.connect(serverEndpoint);
```

---

## Обработка ошибок

```cpp
// кидает исключения
socket.connect(ioService);

// возвращает ошибку в ec
boost::system::error_code ec;
socket.connect(ioService, ec);
```

---

## Создание клиента

```
// создание экземпляра ioService
io_service ioService;
// создание сокета для подключения
ip::tcp::socket socket(ioService);
// создание точки подключения, используется localhost
ip::tcp::endpoint serverEndpoint(ip::address::from_string("127.0.0.1"), 1234);
// подключение к серверу
socket.connect(serverEndpoint);

// чтение из сокета
std::vector<char> buf(128);
size_t len = socket.read_some(buffer(buf));
std::cout.write(buf.data(), len); // выведет "hello world!"
// закрываем сокет
socket.close();
```

---

## Создание сервера

```
io_service ioService;
// создание точки для прослушивания порта 1234
ip::tcp::acceptor acceptor(ioService, ip::tcp::endpoint(ip::tcp::v4(), 1234));
// создаем сокет для принятия нового соединения
ip::tcp::socket socket(ioService);
// принимаем соединение
acceptor.accept(socket);
// запись в сокет
write(socket, buffer("hello world!"));
// закрываем сокет
socket.close();
```

---

## Реальный сервер

- Должен обрабатывать множество клиентских соединений одновременно.
- Отвечает на множественные запросы клиента.

Как сделать?

---

## Сервер

```
// реализация сервера
void serve(unsigned short port) {
  io_service ioService;
  ip::tcp::acceptor acceptor(ioService, ip::tcp::endpoint(ip::tcp::v4(), port));
  while (true) {
    auto socket = std::make_shared<ip::tcp::socket>(ioService);
    acceptor.accept(*socket);
    std::thread([socket] {
      try {
        std::vector<char> buf(1024);
        while (true) {
          // чтение клиентского запроса
          size_t len = socket->read_some(buffer(buf));
          buf.resize(len);
          // обработка buf и формирование ответа response
          auto response = generateResponse(buf);
          write(*socket, buffer(response));
        }
      } catch (std::exception& e) {
        std::cerr << "Error: " << e.what() << std::endl;
      }
    }).detach();
  }
}
```

---

## Клиент

```
Result receiveData(const ip::tcp::endpoint& ep) {
  // соединение к серверу
  io_service ioService;
  ip::tcp::socket socket(ioService);
  socket.connect(ep);
  // формирование запроса в request
  auto request = makeRequest();
  write(socket, buffer(request));
  // чтение данных из сокета
  std::vector<char> buf(1024);
  size_t len = socket.read_some(buffer(buf));
  buf.resize(len);
  // обработка и возвращение результата
  Result result = convertToResult(buf);
  socket.close();
  return result;
}
```

---

## Чтение из сокета

> Внимание!

`socket.read_some(buf)` может вернуть меньшее число байт, чем послал сервер.

Чтобы прочитать строго определенное число байт, нужно использовать функцию:

```cpp
read(socket, buf);
```

---

## Документация

http://boost.cowic.de/rc/pdf/asio_doc.pdf

http://www.boost.org/doc/libs/1_63_0/doc/html/boost_asio.html

---

# Вопросы