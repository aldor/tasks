<!-- $size: B4 -->
<!-- $theme: default -->
<!-- page_number: true -->
<!-- *template: invert -->

# Модели памяти и атомарные переменные

###### Grigory Demchenko (gridem@yandex-team.ru)

---

## Как работает ваш код

![Code Execution](03/code_exec.png)

---

## Исполнение кода

Однопоточная модель:

1. Результат такой же, как если бы вы наблюдали их в последовательности, написанные в коде.
2. Невозможно наблюдать оптимизации, сделанные компилятором или процессором.

---

## Многопоточный код

```cpp
bool f1 = false;
bool f2 = false;

// thread 1
f1 = true;
if (!f2) {
  // critical section
}

// thread 2
f2 = true;
if (!f1) {
  // critical section
}
```

- Оптимизации могут наблюдаться
- Оптимизации могут ломать наивные реализации конкурентных алгоритмов

---

## Memory Model

1. Описывает взаимодействие потоков через память и общие данные.
2. Говорит о том, является ли наша программа корректной с заданным поведением.
3. Запрещает применять компилятору ряд оптимизаций.

---

## Data Race

- __Ячейка памяти__ - объект скалярного типа.
- __Конфликтующие действия__ - 2 и более действий над одной и той же _ячейкой памяти_, и по крайней мере одно из действий - это запись.
- __Data race__ - 2 _конфликтующих действия_ без _happens before_.

> Data race == undefined behavior!

---

## Sequential Consistency

> Leslie Lamport, 1979

Результат любого запуска ровно такой же, как и:
1. Операции всех потоков выполняются в некоторой последовательности.
2. Операции каждого потока проявляются в той же последовательности.

![Sequential Consistency](03/sequential.png)

---

## Блокировки

Самый простой способ реализовать Sequential Consistency - использовать `std::mutex`:

- `.unlock()` синхронизируется с (_synchronize with_)
- `.lock()` на одном и том же экземпляре `std::mutex`.

---

## Синхронизация

C++ определяет следующие операции синхронизации:

![](03/synchronization.png)

- Если `A()` _синхронизуется с_ `B()`,
- то `X()` случится до (_happens before_) `Y()`.

---

## `std::atomic<T>`

- Data race free переменная
- по умолчанию имеет следующую синхронизацию: операция записи синхронизуется с операцией чтения записанного значения
- по умолчанию: sequential consistency
- требует поддержку процессора

---

## Пример синхронизации

```cpp
std::atomic<bool> isDataReady = {false};

// thread 1
prepareData();
isDataReady.store(true);

// thread 2
if (isDataReady.load()) {
  usePreparedData();
}
```

---

## Memory Models

Возможные модели памяти и соответствующие им `memory_order-*`:

![](03/memory_models.png)

---

## Relaxed Memory Model

```cpp
std::atomic<int> x;

x.store(42, std::memory_order_relaxed);
x.load(std::memory_order_relaxed);
```

1. Каждая ячейка памяти имеет определенную последовательность модифицирующих операций.
2. Операции модификации текущего треда над одной и той же ячейкой памяти не переупорядочиваются.

---

## Relaxed Order

![Relaxed Order](03/relaxed.png)

---

## Вопрос про relaxed order

```cpp
atomic<bool> f = {false};
atomic<bool> g = {false};

// thread 1
f.store(true, memory_order_relaxed);
g.store(true, memory_order_relaxed);

// thread 2
while(!g.load(memory_order_relaxed));
assert(f.load(memory_order_relaxed));
```

---

## Acquire/Release

```cpp
x.store(42, memory_order_release);
x.load(memory_order_acquire);
```

1. __Store-release__ _синхронизуется с_ __load-acquire__.
2. Все операции записи в потоке с __store-release__ случаются до (_happens before_) всех операций чтения в __load-acquire__ потоке.

---

## Acquire/Release пример

![acquire](03/acquire.png)

---

## Acquire/Release: race

- Синхронизация в рантайме, а не в коде.
- Требуется проверка, что операция выполнилась.

![acquire race](03/acquire-race.png)

---

## Вопрос про acquire/release

```cpp
atomic<bool> f = {false};
atomic<bool> g = {false};
int n;

// thread 1
n = 42;
f.store(true, memory_order_release);

// thread 2
while(!f.load(memory_order_acquire));
g.store(true, memory_order_release);

// thread 3
while(!g.load(memory_order_acquire));
assert(42 == n);

```

Сработает ли `assert`?

---

## Алгоритм Деккера

```cpp
atomic<bool> f1 = {false};
atomic<bool> f2 = {false};

// thread 1
f1.store(true, memory_order_release);
if (!f2.load(memory_order_acquire)) {
   // critical section
}

// thread 2
f2.store(true, memory_order_release);
if (!f1.load(memory_order_acquire)) {
   // critical section
}
```

Работает?

---

## Алгоритм Деккера

![](03/dekker_acquire.png)

---

## Sequential Consistency и алгоритм Деккера

```cpp
atomic<bool> f1 = {false};
atomic<bool> f2 = {false};

// thread 1
f1.store(true, memory_order_seq_cst);
if (!f2.load(memory_order_seq_cst)) {
   // critical section
}

// thread 2
f2.store(true, memory_order_seq_cst);
if (!f1.load(memory_order_seq_cst)) {
   // critical section
}
```

Работает!

---

## Sequential Consistency и алгоритм Деккера

![](03/dekker_seq.png)

- Глобальный порядок операций чтения и записи

---

## Что использовать?

- Используйте relaxed для обычных счетчиков.
- Если используете платформу x86, то предпочитайте sequential consistency.

---

## Atomic методы

http://en.cppreference.com/w/cpp/atomic/atomic

```cpp
fetch_add
fetch_sub
fetch_and
fetch_or
fetch_xor
operator++
operator--
operator+=
operator-=
operator&=
operator|=
operator^=
```

---

## `atomic_flag`

Атомарный флаг. Идеально подходит для реализации spinlock.

```cpp
operator=
clear
test_and_set
```

---

## CAS

CAS - compare-and-swap операция.

Термин используется в теоретических статьях по lock-free программированию.

В С++ `compare_exchange_weak` или `compare_exchange_strong`.

```cpp
bool atomic::compare_exchange(T& expected, T desired)
{
    if (atomic != expected) {
        expected = atomic;
        return false;
    }
    atomic = desired;
    return true;
}
```

Зачем нужен `compare_exchange_weak` или `compare_exchange_strong`?

---

# Вопросы