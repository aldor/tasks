<!-- $theme: default -->
<!-- page_number: true -->
<!-- *template: invert -->

# Fibers

###### Grigory Demchenko (gridem@yandex-team.ru)

---

## Потоки

Создание потока:

```cpp
std::thread([] {
    std::cout << "hello world!";
});
```

Что не так?

---

## Потоки

```cpp
std::thread([] {
    std::cout << "hello world!";
}).join();
```

Можно использовать `.detach()`, `.join()`.

---

## Работа с текущим потоком

Находятся в `std::this_thread`:

```cpp
yield();
std::thread::id get_id();
void sleep_for(...);
void sleep_until(...);
```

---

## Проблема потоков

> A context switch between threads usually costs thousands of CPU cycles on x86, compared to a fiber switch with less than a hundred cycles.

http://www.boost.org/doc/libs/1_63_0/libs/fiber/doc/html/fiber/overview.html

---

## Проблема асинхронного подхода

> Instead of chaining together completion handlers, code running on a fiber can make what looks like a normal blocking function call.

http://www.boost.org/doc/libs/1_63_0/libs/fiber/doc/html/fiber/overview.html

---

## Преимущества файберов

1. Дешевое переключение контекста.
2. Простота создания асинхронного кода.

---

## Создание файберов

Находятся в `boost::this_fiber` в файле __boost/fiber/all.hpp__:

```cpp
yield();
boost::fibers::id get_id();
void sleep_for(...);
void sleep_until(...);
```

---

## Пример

```cpp
boost::fibers::fiber([] {
    std::cout << "hello world!";
}).join();
```

В чем отличие?

---

## Политики запуска

```cpp
enum class launch {
    dispatch,
    post
};

class fiber
{
    template<typename Fn, typename... Args>
    fiber(Fn&&, Args&& ...);

    template<typename Fn, typename... Args>
    fiber(launch, Fn&&, Args&& ...);
};
```

`dispatch` - синхронный запуск
`post` - асинхронный запуск, по умолчанию

---

## Мьютексы

```cpp
class mutex {
public:
    void lock();
    bool try_lock();
    void unlock();
};

class timed_mutex {
public:
    // lock, try_lock, unlock

    template<typename Clock, typename Duration>
    bool try_lock_until(std::chrono::time_point<
        Clock, Duration> const& timeout_time);

    template<typename Rep, typename Period>
    bool try_lock_for(const std::chrono::duration<
        Rep, Period>& timeout_duration);
};
```

---

## Condition variables

Аналогично `std::condition_variable`, за исключением:

> Neither condition_variable nor condition_variable_any are subject to __spurious wakeup__: `condition_variable::wait()` can only wake up when `condition_variable::notify_one()` or `condition_variable::notify_all()` is called.

http://www.boost.org/doc/libs/1_63_0/libs/fiber/doc/html/fiber/synchronization/conditions.html

---

## Барьеры

```cpp
class barrier {
public:
    explicit barrier(std::size_t);

    bool wait();
};
```

Пример:

```cpp
bf::barrier bar(num);

for (int i = 1; i < num; ++i) {
    thread([&] {
        fiber([&] {
            bar.wait();
        }).join();
    }).detach();
}
bar.wait();
```

---

## Каналы

Типы каналов:

1. Buffered Channel: ограниченная асинхронная MPMC очередь
1. Unbuffered Channel: синхронная MPMC очередь

---

## Buffered Channel

```cpp
buffered_channel<int> chan{2};
fiber send([&] {
    for (int i = 0; i < 5; ++i) {
        chan.push(i);
    }
    chan.close();
});

fiber receive([&] {
    for (auto&& i: chan) {
        std::cout << "received " << i << std::endl;
    }
});

send.join();
receive.join();
```

---

## Unbuffered Channel

```cpp
unbuffered_channel<int> chan;
fiber send([&] {
    for (int i = 0; i < 5; ++i) {
        chan.push(i);
    }
    chan.close();
});

fiber receive([&] {
    for (auto&& i: chan) {
        std::cout << "received " << i << std::endl;
    }
});

send.join();
receive.join();
```

---

## Future

```cpp
promise<int> myPromise;
future<int> myFuture = myPromise.get_future();

myPromise.set_value(42);

assert(myFuture.valid());
assert(myFuture.get() == 42);
```

---

## Асинхронные задачи

```cpp
struct FileReader
{
    future<std::string> read();
};

FileReader reader(...);
auto asyncResult = reader.read();
// perform other tasks ...
auto result = asyncResult.get();
```

---

## Реализация асинхронной задачи

```cpp
future<std::string> FileReader::read()
{
    promise<std::string> promise;
    fiber([promise] {
        // perform actual reading to buffer
        promise.set(buffer);
    });
    return promise.get_future();
}
```

---

## Реализация асинхронной задачи (fixed)

```cpp
future<std::string> FileReader::read()
{
    promise<std::string> promise;
    auto future = promise.get_future();
    fiber([promise = std::move(promise)]() mutable {
        try {
            // perform actual reading to buffer
            promise.set_value(buffer);
        } catch (std::exception& e) {
            promise.set_exception(
                std::current_exception());
        }
    }).detach();
    return future;
}
```

`async` запускает задачу в новом `fiber` и возвращает `future`

---

# Вопросы