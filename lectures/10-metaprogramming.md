# Метапрограммирование

Короткий Фёдор

---

# Метапрограммирование

* **Проблема**: Писать код сложно!

---

# Метапрограммирование

* **Проблема**: Писать код сложно!
* **Решение**: Напишем код который будет писать код за нас :D

_На самом деле нет_

---

# Метапрограммирование

* **Проблема**: Иногда язык С++ не позволяет выразить идею в коде, не нарушая принцип **DRY**
* **Решение**: Нужно расширить язык

---

# Disclaimer

НИ В КОЕМ СЛУЧАЕ, ЕСЛИ НЕ ТРЕБУЕТСЯ ПРИМЕНИМЫМ ЗАКОНОМ ИЛИ ПИСЬМЕННЫМ СОГЛАШЕНИЕМ, НИ ОДИН ИЗ ЛЕКТОРОВ НЕ ОТВЕТСТВЕНЕН ЗА УЩЕРБ, ВКЛЮЧАЯ ОБЩИЙ, КОНКРЕТНЫЙ, СЛУЧАЙНЫЙ ИЛИ ПОСЛЕДОВАВШИЙ УЩЕРБ, ВЫТЕКАЮЩИЙ ИЗ ИСПОЛЬЗОВАНИЯ ИЛИ НЕВОЗМОЖНОСТИ ИСПОЛЬЗОВАНИЯ МЕТАПРОГРАММИРОВАНИЯ (ВКЛЮЧАЯ, НО, НЕ ОГРАНИЧИВАЯСЬ ПОТЕРЕЙ ДАННЫХ ИЛИ НЕВЕРНОЙ ОБРАБОТКОЙ ДАННЫХ, ИЛИ ПОТЕРИ, УСТАНОВЛЕННЫЕ ВАМИ ИЛИ ТРЕТЬИМИ ЛИЦАМИ, ИЛИ НЕВОЗМОЖНОСТЬ МЕТАПРОГРАММЫ РАБОТАТЬ С ДРУГИМИ ПРОГРАММАМИ), ДАЖЕ В СЛУЧАЕ ЕСЛИ ЛЕКТОР БЫЛ ИЗВЕЩЕН О ВОЗМОЖНОСТИ ТАКОГО УЩЕРБА.

---

# Пример 1: Работа с enum

```c++
enum class HTTPCode {
    OK = 200,
    NOT_FOUND = 404
    // 40 more values here 
};

std::string to_string(HTTPCode code);
```

---

# Пример 1: Работа с enum

```c++
enum class HTTPCode {
    OK = 200,
    NOT_FOUND = 404
    // 40 more values here 
};

std::string to_string(HTTPCode code) {
    switch(code) {
    case HTTPCode::OK: return "OK";
    case HTTPCode::NOT_FOUND: return "NOT_FOUND";
    default: return "UNKNOWN";
    }
}
```

---

# Пример 1: Работа с enum

## Что тут происходит на самом деле?

- Есть таблица `(status_code, status_text)`
- Хотим получить из неё 2 вещи
  * C++ `enum`
  * Функцию преобразования `enum -> string`

---

# Пример 1: Работа с enum, XX macro

```c++
#define HTTP_STATUS_CODES \
    XX(200, OK) \
    XX(404, NOT_FOUND)

///////////////////////////////////////////////////////
    
#define XX(status_code, status_text) \
    status_text = status_code,

enum class HTTPCode {
    HTTP_STATUS_CODES
};
    
#undef XX
```

---

# Пример 1: Работа с enum, XX macro

```c++
#define HTTP_STATUS_CODES \
    XX(200, OK) \
    XX(404, NOT_FOUND)

///////////////////////////////////////////////////////
    
#define XX(status_code, status_text) \
    status_text = status_code,

enum class HTTPCode {
    XX(200, OK)
    XX(404, NOT_FOUND)
};
    
#undef XX
```

---

# Пример 1: Работа с enum, XX macro

```c++
#define HTTP_STATUS_CODES \
    XX(200, OK) \
    XX(404, NOT_FOUND)

///////////////////////////////////////////////////////
    
#define XX(status_code, status_text) \
    status_text = status_code,

enum class HTTPCode {
    OK = 200,
    NOT_FOUND = 404,
};
    
#undef XX
```

---

# Пример 1: Работа с enum, XX macro

```c++
#define HTTP_STATUS_CODES \
    XX(200, OK) \
    XX(404, NOT_FOUND)

///////////////////////////////////////////////////////

#define XX(status_code, status_text) \
    case HTTPCode::status_text: return #status_text;
    
std::string to_string(HTTPCode code) {
    switch(code) {
    HTTP_STATUS_CODES
    default: return "UNKNOWN";
    }
}

#undef XX
```

---

# Пример 1: Работа с enum, XX macro

```c++
#define HTTP_STATUS_CODES \
    XX(200, OK) \
    XX(404, NOT_FOUND)

///////////////////////////////////////////////////////

#define XX(status_code, status_text) \
    case HTTPCode::status_text: return #status_text;
    
std::string to_string(HTTPCode code) {
    switch(code) {
    XX(200, OK)
    XX(404, NOT_FOUND)
    default: return "UNKNOWN";
    }
}

#undef XX
```
---

# Пример 1: Работа с enum, XX macro

```c++
#define HTTP_STATUS_CODES \
    XX(200, OK) \
    XX(404, NOT_FOUND)

///////////////////////////////////////////////////////

#define XX(status_code, status_text) \
    case HTTPCode::status_text: return #status_text;
    
std::string to_string(HTTPCode code) {
    switch(code) {
    case HTTPCode::OK: return #OK;
    case HTTPCode::NOT_FOUND: return #NOT_FOUND;
    default: return "UNKNOWN";
    }
}

#undef XX
```

---

# Пример 1: Работа с enum, XX macro

```c++
#define HTTP_STATUS_CODES \
    XX(200, OK) \
    XX(404, NOT_FOUND)

///////////////////////////////////////////////////////

#define XX(status_code, status_text) \
    case HTTPCode::status_text: return #status_text;
    
std::string to_string(HTTPCode code) {
    switch(code) {
    case HTTPCode::OK: return "OK";
    case HTTPCode::NOT_FOUND: return "NOT_FOUND";
    default: return "UNKNOWN";
    }
}

#undef XX
```

---

# Пример 2: Test Auto Discovery

```c++
void TestThis();
void TestThat();

int main() {
    std::cerr << "Testing This" << std::endl;
    TestThis();
    std::cerr << "Finished This" << std::endl;

    std::cerr << "Testing That" << std::endl;
    TestThat();
    std::cerr << "Finished That" << std::endl;

    return 0;
};
```

---

# Пример 2: Test Auto Discovery

## Что тут происходит на самом деле?

- Есть таблица `(test_name, test_fn)`
- Хотим указывать имя теста только в одном месте
- Хотим чтобы тесты сами регистрировались в таблице

---

# Пример 2: Test Auto Discovery

```c++
typedef std::map<std::string, 
                 std::function<void()>>
        TestsTable;

TestsTable ALL_TESTS;
```

---

# Пример 2: Test Auto Discovery

```c++
TestsTable ALL_TESTS;

#define TEST(name) ???

TEST(This) {
    int x = 42;
}
```

---

# Пример 2: Test Auto Discovery

```c++
TestsTable ALL_TESTS;

void TestThis() {
    int x = 42;
}

ALL_TESTS.emplace("This", TestThis);
```

---

# Пример 2: Test Auto Discovery

```c++
TestsTable ALL_TESTS;

void TestThis() {
    int x = 42;
}

int dummy = [] () {
    ALL_TESTS.emplace("This", TestThis);
    return 0;
}();
```

---

# Пример 2: Test Auto Discovery

```c++
TestsTable* AllTests() {
    static TestsTable ALL_TESTS;
    return &ALL_TESTS;
}

void TestThis() {
    int x = 42;
}

int dummy = [] () {
    AllTests()->emplace("This", TestThis);
    return 0;
}();
```

---

# Пример 2: Test Auto Discovery

```c++
TestsTable* AllTests();

void TestThis();

int dummy = [] () {
    AllTests()->emplace("This", TestThis);
    return 0;
}();

void TestThis() {
    int x = 42;
}
```

---

# Пример 2: Test Auto Discovery

```c++
TestsTable* AllTests();

void TestThis();

int dummy_This = [] () {
    AllTests()->emplace("This", TestThis);
    return 0;
}();

void TestThis() {
    int x = 42;
}
```

---

# Пример 2: Test Auto Discovery

```c++
TestsTable* AllTests();

void Test ## Name ();

int dummy_ ## Name = [] () {
    AllTests()->emplace(#Name, Test ## Name);
    return 0;
}();

void Test ## Name () {
    int x = 42;
}
```

---

# Пример 2: Test Auto Discovery

```c++
#define TEST(name)                                 \
                                                   \
void Test ## name ();                              \
                                                   \
int dummy_ ## name = [] () {                       \
    AllTests()->emplace(#name, Test ## name);    \
    return 0;                                      \
}();                                               \
                                                   \
void Test ## name ()

TEST(This) {
    int x = 42;
}
```

---

# Пример 2: Test Auto Discovery

```
TestsTable* AllTests();

void RunAllTests() {
    for (auto& test_case : *AllTests()) {
        std::string name;
        std::function<void()> test_fn;
        // не влезало по длинне
        std::tie(name, test_fn) = test_case;

        std::cerr << "Testing " << name << std::endl; 
        test_fn();
        std::cerr << "Finished " << name << std::endl; 
    }
}
```

---

# Что общего в двух примерах?

- Мы начинаем с обычного кода и убираем повторения
- Логика работы метапрограммы максимально простая, можно описать одним предложением
- Новый механизм используется в `> 10` местах

---

# Пример 3: `List::size()`

```
struct ListHook { ListHook* prev, *next; };

struct List {
    size_t size() const { /* O(n) loop */ }
    ListHook head;
};

struct ListNode {
    ListHook hook;
    void unlink() {
        // move pointers
    }
};
```

---

# Пример 3: `List::size()`

```
struct ListHook {
    ListHook* prev, *next;
    size_t *size;
};

struct List {
    size_t size() const { return size_; }

    ListHook head;
    size_t size_;
};

struct ListNode {
    ListHook hook;
    void unlink() {
        // move pointers
        --(*hook.size);
    }
};
```
---

# Пример 3: `List::size()`

```
struct ListHook { ListHook* prev, *next; };

struct List {
    size_t size() const;

    ListHook head;
    size_t size;
};

struct ListNode {
    ListHook hook;
    void unlink();
};
```

---

# Пример 3: `List::size()`

## Что тут происходит на самом деле?

* Мы хотим статически (на уроне типа) настраивать поведение структуры данных

---

# Пример 3: `List::size()`

```
struct ListHook {
    ListHook* prev, *next;
    OptionalSizePtr size;
};

struct List {
    size_t size() const;

    ListHook head;
    OptionalSizeField size_;
};

struct ListNode {
    ListHook hook;
    void unlink();
};
```

---

# Пример 3: `List::size()`

```
struct ListHook {
    ListHook* prev, *next;
};

struct List {
    size_t size() const;

    ListHook head;
    OptionalSizeField size_;
};

struct ListNode {
    ListHook hook;
    OptionalSizePtr size;
    void unlink();
};
```

---

# Пример 3: `List::size()`

```c++
struct List {
    size_t size() const {
        if (HasOptionalSizeField) { return size_; }
        else { /* O(n) loop */ }
    }

    ListHook head;
    OptionalSizeField size_;
};
```

---

# Пример 3: `List::size()`

```c++
class ConstSizePolicy {
    size_t size_;
    size_t size(const ListHook&) { return size_; }
};
class CompureSizePolicy {
    size_t size(const ListHook& head) {
        // O(n) loop
    }
}

template<class SizePolicy>
class List : public SizePolicy {
	size_t size() { return SizePolicy::size(); }
};
```

---

# Пример 3: `List::size()`

```c++
struct ListNode {
    ListHook hook;
    OptionalSizePtr size;
    void unlink() {
        // move pointers        
        if (HasSize) { --size; }
    }
};
```

---

# Пример 3: `List::size()`

```c++
struct ComputeSizeNodePolicy {
    void on_unlink();
};
struct ConstSizeNodePolicy {
    size_t* size;
    void on_unlink() {
        --(*size);
    }
};

template<class SizePolicy>
struct ListNode : public SizePolicy {
    void unlink() {
        --(*size);
        SizePolicy::on_unlink();
    }
};
```

---

# Пример 3: `List::size()`

```
struct ListHook {
    ListHook* prev, *next;
};

template<class SizePolicy>
struct List {
    size_t size() const;

    ListHook head;
    OptionalSizeField size_;
    
    ListNode* first(); // ???
};

template<class NodeSizePolicy>
struct ListNode : public NodeSizePolicy {
    ListHook hook;
    void unlink();
};
```

---

# Пример 3: `List::size()`

```
struct ConstSizePolicy {
    typedef ConstSizeNodePolicy NodePolicy;
};

template<class SizePolicy>
struct List {
    typedef ListNode<typename SizePolicy::NodePolicy>
            Node;

    Node first();
};
```

---

# Пример 3: `List::size()`

## На что стоит обратить внимание

- Мы "заархивировали код"
- Читать его стало гораздо сложнее
- Множество политик "закрыто" для расширения
- Сторонний код может попытаться добавить политику, но ничём хорошим это не закончится

---

# Пример 4: Сериализация данных

```c++
struct Request {
    std::string user_id;
    std::string type;
    std::string query;
};

Json::Value ToJson(const Request& req);
Request FromJson(const Json::Value& json);
```

---

# Пример 4: Сериализация данных

```c++
struct Request { /** fields */ };

Json::Value ToJson(const Request& req) {
    Json::Value value;
    value["user_id"] = user_id;
    value["type"] = type_query;
    value["query"] = query;
    return value;
}

Request FromJson(const Json::Value& json) {
    Request req;
    req.user_id = json["user_id"].as<std::string>();
    req.type = json["type"].as<std::string>();
    req.query = json["type"].as<std::string>(); // OOPS
    return req;
}
```

---

# Пример 4: Сериализация данных

## Что происходит на самом деле

* Есть объект, состоящий из набора полей
* Каждое поле имеет имя и тип
* Есть правила преобразования Json <-> Object

---

# Пример 4: Сериализация данных

```c++
struct Request { /** fields */ };

Json::Value ToJson(const Request& req) {
    Json::Value json;
    for (auto& fieldDesc : ListField(req)) {
        json[fieldDesc.name] = fieldDesc.value;
    }
    return json;
}
```

---

# Пример 4: Сериализация данных

```c++
struct FieldDescriptor {
    std::string name;
    std::string *value;
};

struct Request {
    std::string user, query;

    void visit(std::vector<FieldRescriptor>* table) {
        table->push_back("user", &user);
        table->push_back("query", &query);
    }
};
```

---

# Пример 4: Сериализация данных

```c++
struct FieldDescriptor {
    std::string name;
    std::string Request::* field_ptr;
};

struct Request {
    std::string user, query;

    static void visit(
        std::vector<FieldRescriptor>* table
    ) {
        table->push_back("user", &Request::user);
        table->push_back("query", &Request::query);
    }
};
```

---

# Пример 4: Сериализация данных

```c++
struct FieldDescriptor {
    std::string name;
    std::string Request::* field_ptr;
};

struct Request {
    std::string user, query;

    BEGIN_DECLARE_JSON_FIELD()
        JSON_FIELD(user)
        JSON_FIELD(query)
    END_DECLARE_JSON_FIELD();
};
```

---

# Минусы метапрограммирования

![babylon](10-babylon-pic.jpg)
