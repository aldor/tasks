<!-- $size: B4 -->
<!-- $theme: default -->
<!-- page_number: true -->
<!-- *template: invert -->

# Асинхронность в boost.asio

###### Grigory Demchenko (gridem@yandex-team.ru)

---

## Синхронность и асинхронность

_Синхронный_ вызов - результат доступен сразу после завершения.

_Асинхронный_ вызов - результат будет доступен позже. Точка вызова и точка получения результата разнесены.

> Когда и где будет доступен результат?

---

## Асинхронный результат

Варианты:

1. Callback.
2. Future.
3. Subscription.

boost.asio использует 1-й вариант.

---

## Мотивация

Зачем нужна асинхронность?

---

## Мотивация

- Экономия ресурсов.
- Более производительное и масштабируемое решение.

---

## Архитектура

![архитектура](04/overview.png)

---

## Подключение к серверу

_Синхронное_:

```cpp
boost::system::error_code ec;
socket.connect(serverEndpoint, ec);
```

_Асинхронное_:

```cpp
// обработчик завершения подключения
void completionHandler(const boost::system::error_code& ec);

socket.async_connect(serverEndpoint, completionHandler);
```

---

## Стадии асинхронной обработки

Запуск асинхронной операции:

![stage1](05/async_op1.png)

---

## Стадии асинхронной обработки

Получение результата операции:

![stage2](05/async_op2.png)

---

## Проактор

`boost.asio` реализует проакторную модель:

![proactor](05/proactor.png)

---

## Event Loop

```cpp
io_service ioService;
ip::tcp::socket socket(ioService);
socket.async_connect(
    serverEndpoint,
    [](const boost::system::error_code& ec){
        if (!ec) {
            // ok
        } else {
            // fail
        }
    });
ioService.run();
```

Запуск event loop:

1. `io_service::run()` - завершается при пустой очереди.
2. `io_service::run_one()` - завершается после запуска одного обработчика.

---

## Работа с буфером

2 типа буфера, которые можно представить как:

```cpp
typedef std::pair<void*, std::size_t> mutable_buffer;
typedef std::pair<const void*, std::size_t> const_buffer;
```

`mutable_buffer` используется для чтения данных из сокета.
`const_buffer` используется для записи данных в сокет.

Функция `buffer` создает нужный тип из других типов: `std::vector<T>`, `std::string`, `boost::array`.

---

## Асинхронная запись

Наивный подход:

```cpp
int value = 42;
async_write(
    socket,
    buffer(&value, sizeof(int)),
    [](const boost::system::error_code& ec, size_t size) {
        if (!ec) {
            // ok
        } else {
            // fail
        }
    });
```

---

## Асинхронная запись

Правильный подход:

```cpp
auto value = std::make_shared<int>(42);
async_write(
    socket,
    buffer(value.get(), sizeof(int)),
    [value](const boost::system::error_code& ec, size_t size) {
        if (!ec) {
            // ok
        } else {
            // fail
        }
    });
```

---

## Асинхронное чтение

Наивный подход:

```cpp
int value;
async_read(
    socket,
    buffer(&value, sizeof(int)),
    [](const boost::system::error_code& ec, size_t size) {
        if (!ec) {
            // ok
        } else {
            // fail
        }
    });
```

---

## Асинхронное чтение

Правильный подход:

```cpp
auto value = std::make_shared<int>();
async_read(
    socket,
    buffer(value.get(), sizeof(int)),
    [value](const boost::system::error_code& ec, size_t size) {
        if (!ec) {
            // ok
        } else {
            // fail
        }
    });
```

---

## Обработка таймаутов

```cpp
io_service ioService;
ip::tcp::socket socket(ioService);
socket.async_connect(
    serverEndpoint,
    [](const boost::system::error_code& ec){
        if (!ec) {
            // ok
        } else {
            // fail
        }
    });
```

Если сервер недоступен, то будем бесконечно пытаться.
Хотим на каждое действие поставить таймер и прерывать операцию.

---

## Обработка таймаутов

```
// использование deadline_timer
io_service ioService;

auto socket = std::make_shared<ip::tcp::socket>(ioService);
auto timer = std::make_shared<deadline_timer>(
    ioService,
    boost::posix_time::seconds(1));

socket->async_connect(
    serverEndpoint,
    [timer](const boost::system::error_code& ec){
        if (!ec) {
            timer->cancel();
        } else {
            // fail
        }
    });

timer->async_wait(
    [socket](const boost::system::error_code& ec) {
        if (!ec) {
            socket->close();
        }
    });
```

---

## Время жизни объектов

Важно следить за временем жизни объектов.

Возможные стратегии:

1. `shared_ptr`
1. `enable_shared_from_this<T>` + `::shared_from_this`
2. Finite State Machine

---

## Документация

http://boost.cowic.de/rc/pdf/asio_doc.pdf

http://www.boost.org/doc/libs/1_63_0/doc/html/boost_asio.html

---

# Вопросы