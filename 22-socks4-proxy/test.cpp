#include <gtest/gtest.h>

#include <glog/logging.h>

#include <thread>
#include <socks4.h>

static int dummy = [] {
    FLAGS_logtostderr = 1;
    google::InitGoogleLogging("test");
    return 1;
}();

struct EchoConn : public std::enable_shared_from_this<EchoConn> {
    EchoConn(boost::asio::io_service& io_service) : socket(io_service) {
        buffer.resize(1024);
    }

    boost::asio::ip::tcp::socket socket;
    std::vector<char> buffer;

    void Start() {
        StartRead();
    }

    void StartRead() {
        auto that = shared_from_this();
        socket.async_read_some(boost::asio::buffer(buffer),
            [this, that] (const boost::system::error_code& error,
                          size_t bytes_transferred) {
                if (bytes_transferred == 0 || error) {
                    Disconnect();
                    return;
                }

                StartWrite(bytes_transferred);
            });
    }

    void StartWrite(size_t size) {
        auto that = shared_from_this();
        boost::asio::async_write(socket, boost::asio::buffer(buffer.data(), size),
            [this, that] (const boost::system::error_code& error,
                          size_t bytes_transferred) {
                if (error) {
                    Disconnect();
                    return;
                }

                StartRead();
            });
    }

    void Disconnect() {
        LOG(INFO) << "Echo client " << socket.remote_endpoint() << " disconnected";
    }
};

class EchoServer : public std::enable_shared_from_this<EchoServer> {
public:
    EchoServer(boost::asio::io_service& io_service,
               boost::asio::ip::tcp::endpoint endpoint)
        : acceptor_(io_service, endpoint) {}

    void StartAccept() {
        next_conn = std::make_shared<EchoConn>(acceptor_.get_io_service());

        auto that = shared_from_this();        
        acceptor_.async_accept(next_conn->socket,
            [this, that] (const boost::system::error_code& ec) {
            if (ec) return;

            LOG(INFO) << "Echo server accepted " << next_conn->socket.remote_endpoint();

            next_conn->Start();
            StartAccept();
        });
    }

private:
    boost::asio::ip::tcp::acceptor acceptor_;
    
    std::shared_ptr<EchoConn> next_conn;
};


class Socks4Test : public ::testing::Test {
public:
    boost::posix_time::time_duration timeout;

    boost::asio::io_service io_service;
    boost::asio::ip::tcp::endpoint proxy_a_endpoint, proxy_b_endpoint;
    boost::asio::ip::tcp::endpoint closed_endpoint, echo_endpoint;

    std::shared_ptr<EchoServer> echo_server;
    
    Socks4Test() {
        timeout = boost::posix_time::seconds(1);
    
        auto localhost = boost::asio::ip::address::from_string("127.0.0.1");
        proxy_a_endpoint = { localhost, 33334 };
        proxy_b_endpoint = { localhost, 33335 };

        closed_endpoint = { localhost, 44444 };
        echo_endpoint = { localhost, 44444 };
        echo_server = std::make_shared<EchoServer>(io_service, echo_endpoint);
        echo_server->StartAccept();

        worker = std::thread([&] {
            try {
                io_service.run();
            } catch (const std::exception& e) {
                LOG(ERROR) << "io_service stoped: {}" << e.what();
            }
        });
    }

    ~Socks4Test() {
        io_service.stop();
        worker.join();
    }

    std::thread worker;
};

void TestEchoConnection(boost::asio::ip::tcp::socket& socket) {
    std::vector<char> sent(1024), received(1024);
    srand(42);
    for (int i = 0; i < 10; ++i) {
        for (char& c : sent) { c = rand(); }

        boost::asio::write(socket, boost::asio::buffer(sent));
        boost::asio::read(socket, boost::asio::buffer(received));

        ASSERT_EQ(sent, received);
    }

    socket.shutdown(boost::asio::ip::tcp::socket::shutdown_send);
    boost::system::error_code ec;
    ASSERT_EQ(0, socket.read_some(boost::asio::buffer(received), ec));
    if (ec != boost::asio::error::eof) {
        throw boost::system::system_error(ec);
    }
}

TEST_F(Socks4Test, ClientNoProxy) {
    boost::asio::ip::tcp::socket socket(io_service);

    ConnectProxyChain(socket, {}, echo_endpoint);

    TestEchoConnection(socket);
}

TEST_F(Socks4Test, SingleProxy) {
    auto proxy_a = std::make_shared<Socks4Proxy>(io_service, proxy_a_endpoint, timeout);
    proxy_a->startAccept();
    
    boost::asio::ip::tcp::socket socket(io_service);
    ProxyParams proxy_a_params{proxy_a_endpoint, "root"};
    ConnectProxyChain(socket, { proxy_a_params }, echo_endpoint);

    TestEchoConnection(socket);
}

TEST_F(Socks4Test, LongProxyChain) {
    auto proxy_a = std::make_shared<Socks4Proxy>(io_service, proxy_a_endpoint, timeout);
    proxy_a->startAccept();
    
    auto proxy_b = std::make_shared<Socks4Proxy>(io_service, proxy_b_endpoint, timeout);
    proxy_b->startAccept();

    std::vector<ProxyParams> proxy_chain;
    for (int i = 0; i < 16; ++i) {
        auto endpoint = ((i * i) % 2) ? proxy_a_endpoint : proxy_b_endpoint;
        proxy_chain.push_back({ endpoint, "user" + std::to_string(i) });
    }

    boost::asio::ip::tcp::socket socket(io_service);
    ConnectProxyChain(socket, proxy_chain, echo_endpoint);
    
    TestEchoConnection(socket);
}
