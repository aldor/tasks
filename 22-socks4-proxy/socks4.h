#include <stdint.h>

#include <set>

//#define BOOST_ASIO_ENABLE_HANDLER_TRACKING
#include <boost/asio.hpp>

void Socks4Handshake(boost::asio::ip::tcp::socket& conn,
                     const boost::asio::ip::tcp::endpoint& addr,
                     const std::string& user);

struct ProxyParams {
    boost::asio::ip::tcp::endpoint endpoint;
    std::string user;
};
                     
void ConnectProxyChain(boost::asio::ip::tcp::socket& socket,
                       const std::vector<ProxyParams>& proxy_chain,
                       boost::asio::ip::tcp::endpoint destination);

                       
class Socks4Proxy : public std::enable_shared_from_this<Socks4Proxy> {
public:
    Socks4Proxy(boost::asio::io_service& io_service,
                boost::asio::ip::tcp::endpoint at,
                boost::posix_time::time_duration timeout);
    ~Socks4Proxy() {}

private:
};
