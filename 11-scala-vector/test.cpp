#include <gtest/gtest.h>
#include <immutable_vector.h>
#include <string>
#include <vector>
#include <random>
#include <thread>
#include <mutex>

template<typename T>
std::vector<T> get_values(const ImmutableVector<T>& data) {
    std::vector<T> result;
    result.reserve(data.size());
    for (size_t i = 0; i < data.size(); ++i)
        result.push_back(data.get(i));
    return result;
}

std::vector<int> make_range(int count) {
    std::vector<int> result(count);
    for (int i = 0; i < count; ++i)
        result[i] = i;
    return result;
}

TEST(Correctness, Constructors) {
    ImmutableVector<int> empty;
    ASSERT_EQ(0, empty.size());

    ImmutableVector<int> from_list{1, 2, 3};
    ASSERT_EQ(3, from_list.size());
    std::vector<int> to_check{1, 2, 3};
    ASSERT_EQ(to_check, get_values(from_list));

    std::vector<std::string> origin{"aba", "caba"};
    ImmutableVector<std::string> range(origin.begin(), origin.end());
    ASSERT_EQ(2, range.size());
    ASSERT_EQ(origin, get_values(range));
}

TEST(Correctness, ChangeSize) {
    const int iterations_count = 1000;
    ImmutableVector<int> data;
    std::vector<ImmutableVector<int>> versions;
    versions.reserve(iterations_count);
    for (int i = 0; i < iterations_count; ++i) {
        data = data.push_back(i);
        versions.push_back(data);
        ASSERT_EQ(versions.back().size(), i + 1);
        ASSERT_EQ(make_range(i + 1), get_values(versions.back()));
    }
    for (int i = 0; i < iterations_count; ++i) {
        data = data.pop_back();
        ASSERT_EQ(data.size(), iterations_count - i - 1);
        ASSERT_EQ(make_range(iterations_count - i - 1), get_values(data));
    }
}

TEST(Correctness, SetGet) {
    const int iterations_count = 1000;
    ImmutableVector<int> data(50);
    std::vector<int> vector_data(50);
    std::mt19937 gen(7346475);
    std::uniform_int_distribution<int> dist(0, data.size() - 1);
    for (int i = 0; i < iterations_count; ++i) {
        int index = dist(gen);
        data = data.set(index, i + 1);
        vector_data[index] = i + 1;
        for (int j = 0; j < 10; ++j) {
            int check_index = dist(gen);
            ASSERT_EQ(vector_data[check_index], data.get(check_index));
        }
    }
    ASSERT_EQ(vector_data, get_values(data));
}

TEST(Concurrency, NoSync) {
    ImmutableVector<int> data;
    int threads_count = std::thread::hardware_concurrency();
    std::vector<std::thread> threads;
    threads.reserve(threads_count);
    for (int i = 0; i < threads_count; ++i) {
        threads.emplace_back([&data]() {
            auto my_data = data;
            std::vector<int> ok_data;
            for (int i = 0; i < 100; ++i) {
                ok_data.push_back(i);
                my_data = my_data.push_back(i);
                ASSERT_EQ(ok_data, get_values(my_data));
            }
        });
    }

    for (auto& thread : threads)
        thread.join();
}

TEST(Concurrency, Sync) {
    int threads_count = std::thread::hardware_concurrency();
    int iterations_count = 1000;
    auto fill = make_range(iterations_count);
    ImmutableVector<int> data(fill.begin(), fill.end());
    std::vector<std::thread> threads;
    threads.reserve(threads_count);
    std::mutex mutex;
    for (int i = 0; i < threads_count; ++i)
        threads.emplace_back([&data, &mutex, iterations_count, i]() {
            std::mt19937 gen(7464754 + i);
            for (int j = 0; j < 10; ++j) {
                mutex.lock();
                auto my_data(data);
                mutex.unlock();
                std::uniform_int_distribution<int> dist(0, my_data.size() - 1);
                for (int k = 0; k < iterations_count; ++k) {
                    int index = dist(gen);
                    ASSERT_EQ(index, my_data.get(index));
                }
            }
        });
    for (int i = 0; i < 2; ++i)
        threads.emplace_back([&data, &mutex]() {
            for (int j = 0; j < 50; ++j) {
                std::lock_guard<std::mutex> lock(mutex);
                data = data.pop_back();
            }
        });

    for (auto& thread : threads)
        thread.join();
}
