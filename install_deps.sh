#!/bin/sh

set -e -x

apt-get update && apt-get install -y curl

curl -s https://packagecloud.io/install/repositories/shad-cpp/course/script.deb.sh | bash

apt-get update && apt-get install -y \
    gdb \
    build-essential \
    cmake \
    ninja-build \
    clang-format \
    clang-tidy \
    g++ \
    libboost1.62-dev \
    libboost-fiber1.62-dev \
    libboost-system1.62-dev \
    libboost-context1.62-dev \
    libboost-coroutine1.62-dev \
    libgtest-dev \
    libbenchmark-dev \
    libsnappy-dev \
    libshad-gtest \
    libgoogle-glog-dev \
    sudo \
    strace
