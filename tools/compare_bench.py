#!/usr/bin/env python3
"""
compare_bench.py - Compare results of benchmarks and report the difference.
"""
import sys
import json
import pprint
import os

def usage():
    print('compare_bench.py <baseline_result.json> <solution_result.json>...')
    exit(1)

class Colors:
    BLUE = '\033[34m'
    GREEN = '\033[92m'
    RED = '\033[91m'
    YELLOW = '\033[33m'
    
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

if not os.isatty(1):
    Colors.BLUE = Colors.GREEN = Colors.RED = Colors.YELLOW = ''
    
def format_cell_old(text, color=""):
    return color + "{:>10}".format(text) + Colors.ENDC


def results_name(i):
    if i == 0:
        return "Baseline"
    else:
        return "Solution " + str(i)

    
def format_difference(baseline, solution, time_unit):
    if baseline == 0:
        return ""
    speedup = 100 * (solution - baseline) / baseline
    
    if baseline < solution:
        color = Colors.RED
    else:
        color = Colors.GREEN

    return (format_cell_old(str(solution) + time_unit, color) +
            format_cell_old("{:+.1f}%".format(speedup), color + Colors.BOLD))


def cell_len(cell):
    if cell is None:
        return 0
    if isinstance(cell, tuple):
        cell = cell[0]
    return len(cell)


def format_cell(cell, size):
    if cell is None:
        return " " * size
    
    color = ""
    if isinstance(cell, tuple):
        color = cell[1]
        cell = cell[0]
    return color + " " * (size - len(cell)) + cell + Colors.ENDC


def format_table(table):
    row_sizes = [0] * len(table[0])
    for row in table:
        empty_rows = 0
        for i in range(len(row)):
            if row[i] is not None:
                min_len = 2 + (cell_len(row[i]) // (empty_rows + 1))
                row_sizes[i] = max(min_len, row_sizes[i])
                empty_rows = 0
            else:
                empty_rows += 1

    for row in table:
        line = ""
        row_size = 0
        for i, cell in enumerate(row):
            row_size += row_sizes[i]
            if cell is None:
                continue

            line += format_cell(cell, row_size)
            row_size = 0

        print(line)


def build_report(results):
    header = [None, None, (results_name(0), Colors.BLUE)]    
    header2 = [None, ("CpuTime", Colors.BOLD), ("RealTime", Colors.BOLD)]
    for i in range(1, len(results)):
        header += [None, None, None, (results_name(i), Colors.BLUE)]
        header2 += [None, header2[1], None, header2[2]]

    report_rows = [header, header2]    
    for i in range(len(results[0])):
        name = results[0][i]['name']
        for res in results:
            if res[i]['name'] != name:
                raise ValueError("Benchmark names don't match")

        report_row = []
        report_row.append(results[0][i]['name'])
        
        time_units = results[0][i]['time_unit']
        baseline_cputime = results[0][i]['cpu_time']
        baseline_realtime = results[0][i]['real_time']
        report_row += [            
            (str(baseline_cputime) + time_units, Colors.YELLOW),
            (str(baseline_realtime) + time_units, Colors.YELLOW),
        ]

        def format_speedup(baseline, solution):
            if baseline == 0:
                return ["", ""]
            speedup = 100 * (solution - baseline) / baseline

            if abs(baseline - solution) < 2:
                color = Colors.YELLOW
            elif baseline < solution:
                color = Colors.RED
            else:
                color = Colors.GREEN

            return [(str(solution) + time_units, color),
                    ("{:+.1f}%".format(speedup), color + Colors.BOLD)]
        
        for solution in range(1, len(results)):
            solution_cputime = results[solution][i]['cpu_time']
            solution_realtime = results[solution][i]['real_time']

            report_row += format_speedup(baseline_cputime, solution_cputime)
            report_row += format_speedup(baseline_realtime, solution_realtime)
                
        report_rows.append(report_row)
    return report_rows


def main():
    if '--help' in sys.argv or len(sys.argv) < 3:
        usage()

    results_files = sys.argv[1:]
    print("Comparing results:")
    for i in range(len(results_files)):
        print("  " + Colors.BLUE + "{:>10}".format(results_name(i)) + Colors.ENDC + " - " + results_files[i])
    print()
    results = [json.load(open(f))["benchmarks"] for f in results_files]

    report_rows = build_report(results)

    format_table(report_rows)
    
if __name__ == '__main__':
    main()
