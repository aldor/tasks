#!/usr/bin/python3
import subprocess
import sys
import os
import pwd
import grp
import resource

ENV_WHITELIST = ["PATH"]
MEMORY_LIMIT = 1024 ** 3 # 1 GB
TIMEOUT_SECONDS = 20
NUM_CPUS = 4

LIMIT_MEMORY=True

def drop_priv():
    uid = pwd.getpwnam("nobody").pw_uid
    gid = grp.getgrnam("nogroup").gr_gid

    if LIMIT_MEMORY:
        resource.setrlimit(resource.RLIMIT_AS, (MEMORY_LIMIT, MEMORY_LIMIT))
    else:
        print("WARNING: Not limiting memory usage", file=sys.stderr)
    resource.setrlimit(resource.RLIMIT_CPU, (NUM_CPUS * TIMEOUT_SECONDS, NUM_CPUS * TIMEOUT_SECONDS))
    resource.setrlimit(resource.RLIMIT_NPROC, (1000, 1000))

    try:
        os.setgroups([])
        os.setresgid(gid, gid, gid)
        os.setresuid(uid, uid, uid)
    except:
        print("WARNING: UID and GID change failed, running with current user", file=sys.stderr)

    env = os.environ.copy()
    os.environ.clear()
    for variable in ENV_WHITELIST:
        os.environ[variable] = env[variable]
    
def main():
    if sys.argv[1] == "--no-mem-limit":
        global LIMIT_MEMORY
        LIMIT_MEMORY=False
        sys.argv.pop(1)
    
    subprocess.run(sys.argv[1:],
                   preexec_fn=drop_priv,
                   timeout=TIMEOUT_SECONDS,
                   check=True,)

if __name__ == "__main__":
    main()
