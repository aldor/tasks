# Coroutine

В этой задаче вам предстоит реализовать сопрограмму.

Чтобы понять, что это такое, рассмотрим пример:

```cpp
void coroFn() {
    std::cout << 1;
    suspend();
    std::cout << 2;
}

std::cout << 3;
Coroutine c(coroFn);
std::cout << 4;
c.resume();
std::cout << 5;
```

На экране должны быть выведено `31425`.

Т.е. в каждый момент сопрограмма может остановить свое выполнение вызовом `suspend()` и переключиться на вызывающую функцию. Чтобы продолжить исполнения с точки последнего вызова `suspend()` необходимо на экземпляре объекта `Coroutine` позвать метод `resume()`.

Стоит обратить внимание, что функция `suspend()` не является методом класса `Coroutine`. Возникает вопрос: "а как узнать экземпляр текущей сопрограммы?" Для этого можно воспользовать thread local storage переменной:

```cpp
thread_local Coroutine* currentCoroutine = nullptr;
```

Перед входом в сопрограмму необходимо поменять значение этой переменной, а после выхода - сбросить.

## Context

Для решения этой задачи нужно использовать boost версии 1.62. Мы
собрали нужные пакеты для убунты 16.10, 16.04 и 14.04. Для того чтобы
установить boost, выполните команду `sudo apt-get update && sudo apt-get install libboost1.62-dev libboost-context1.62-dev`

Для реализации понадобится манипуляция с контекстом исполнения, т.е. мы будем переключать контекст с одного на другой. Подробности смотрите в лекции.

Для реализации необходимо использовать заголовочный файл `boost/context/detail/fcontext.hpp`. В нем определены 2 метода и 2 типа:

```cpp
typedef void* fcontext_t;

struct transfer_t {
    fcontext_t  fctx;
    void    *   data;
};

transfer_t jump_fcontext( fcontext_t const to, void * vp);
fcontext_t make_fcontext( void * sp, std::size_t size, void (* fn)( transfer_t) );
```

`make_fcontext` создает необходимый контекст `fcontext_t`. При этом контекст вызываемого передается внутрь колбека через входной параметр `transfer_t`. Указатель `void * vp` будет записан внутрь `transfer_t::data`. Таким образом можно передать часть данных (например, указатель на объект `Coroutine`), а `transfer_t::fctx` использовать для сохранения вызываемого контекста. `void * sp` и `std::size_t size` - это указатель на вершину стека и размер стека соответственно. Вершина стека начинается с верхних адресов, т.е. если используется `std::vector`, то необходимо передавать указатель на конец.

Для реализации `suspend` и `resume` надо использовать:

```cpp
// Resume:
coroCtx = jump_fcontext(coroCtx, ptr).fctx;

// Suspend:
extCtx = jump_fcontext(extCtx, ptr).fctx;
```

Идея в том, что `jump_fcontext` в качестве входного параметра принимает контекст, _куда_ мы переключаемся. После возврата возвращается контекст _откуда_ мы переключились.

### Вопросы

* Как реализованы функции `jump_fcontext` и `make_fcontex` и почему?
* Зачем использовать `thread_local` и как это работает в многопоточной среде? Что такое `errno`?

### Полезные ссылки

- [boost/context/detail/fcontext.hpp](https://github.com/boostorg/context/blob/develop/include/boost/context/detail/fcontext.hpp)
- [Thread local storage](https://en.wikipedia.org/wiki/Thread-local_storage)

### Ограничения

1. Запрещено использовать класс `execution_context`.
1. Запрещено использовать `boost.coroutine`.
1. Запрещено использовать `boost.fiber`.

### Лидерборд
Правила оценивания обычные (см. [инструкцию](https://gitlab.com/shad-cpp-course/tasks/blob/master/docs/setup.md)).
