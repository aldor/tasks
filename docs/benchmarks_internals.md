# Как работают микро-бенчмарки

Перед тем как читать этот файл, прочитайте [README библиотеки google-benchmark на github](https://github.com/google/benchmark)

В этом документе подробно разбирается как работает библиотека google/benchmark.
Мы будем разбирать исходный код, поэтому советую вам склонировать репозиторий
себе на локальную машину и читать код в своём любимом редакторе.

## Основной цикл измерения

Основной цикл тестирования находится в функции `RunBenchmark()` в `src/benchmark.cc`. 
  - Откройте файл и найдите там функцию.
  - Не нужно глазами просматривать весь файл, пользуйтесь текстовым поиском!

Самый внешний цикл внутри этой функции отвечает за повторные измерения и нам
пока не интересен. Внутренний цикл отвечает за подбор количества итераций цикла
внутри тестовой функции (переменная `iters`). `iters` должен быть достаточно большим, 
чтобы измерения были точными, но и не слишком большим, чтобы вы не устали ждать.
  - С какого значения `iters` начинается подбор? Как он растёт?

В самом начале цикла запускается код теста. (Код до вызова `WaitForAllThreads()`).
  - Сколько потоков запускается на каждой итерации цикла?

Весь оставшийся код в цикле занимается подбором оптимального количества
итераций цикла и агрегацией статистики.
  - Запустите бенчмарк на hash-table с опцией `--v=2` и прочитайте лог работы этого цикла.
  - В какой переменной хранятся результаты измерений?

## Работа одного бенчмарка

Если вы внимательно читали документацию, то должны были заметить что библиотека
измеряет не всё время работы функции, а только время работы внутреннего цикла.

```c++
static void BM_memcpy(benchmark::State& state) {
  char* src = new char[state.range(0)]; // *
  char* dst = new char[state.range(0)]; // | Set up code not measured
  memset(src, 'x', state.range(0));     // *

  while (state.KeepRunning())
    memcpy(dst, src, state.range(0)); // Only this code is measured

  state.SetBytesProcessed(int64_t(state.iterations()) * // *
                          int64_t(state.range(0)));     // | Tear down code not measured
  delete[] src;                                         // |
  delete[] dst;                                         // *
}
```

Это значит, что библиотека должна запускать таймер в момент первого вызова
`KeepRunning()` и останавливать таймер в тот момент, когда `KeepRunning()`
вернул `false`.

Мы хотим узнать как реализована функция `KeepRunning()`.
  - Найдите реализацию этой функции с помощью утилиты `ack-grep`.
    1. Запустите команду `ack-grep KeepRunning` в корне проекта.
    2. Разберитесь в каких файлах упоминается `KeepRunning`. Какие файлы вам точно не интресны?
    3. Какие строчки похожи на вызовы метода, а где его определение?
    4. Откройте файл в редакторе и убедитесь что вы нашли то что нужно.

Вот тело метода:

```c++
bool State::KeepRunning() {
  if (BENCHMARK_BUILTIN_EXPECT(!started_, false)) {
    StartKeepRunning();
  }
  bool const res = total_iterations_++ < max_iterations;
  if (BENCHMARK_BUILTIN_EXPECT(!res, false)) {
    FinishKeepRunning();
  }
  return res;
}
```

Тут видно, что `KeepRunning()` делает какую-то работу только на первой и
последней итерации. Остальное время функция просто увеличивает счётчик и делает
2 проверки. Метод вынесен в заголовочный файл и в нем используются макросы
`BUILTIN_EXPECT`, чтобы компилятор смог заинлайнить тело метода и
оптимизировать common-case когда итерация не первая и не последняя.

  - Найдите реализации методов StartKeepRunning и FinishKeepRunning.

```c++
void State::StartKeepRunning() {
  CHECK(!started_ && !finished_);
  started_ = true;
  manager_->StartStopBarrier();
  if (!error_occurred_) ResumeTiming();
}

void State::FinishKeepRunning() {
  CHECK(started_ && (!finished_ || error_occurred_));
  if (!error_occurred_) {
    PauseTiming();
  }
  // Total iterations now is one greater than max iterations. Fix this.                                                                                                  
  total_iterations_ = max_iterations;
  finished_ = true;
  manager_->StartStopBarrier();
}
```

Мы видим, что измерение времени происходит в методах StartTiming и StopTiming.
Значит что библиотека измеряет время работы цикла `KeepRunning` целиком, а
потом делит это время на количество итераций. Например, она не сможет показать
вам что `push_back()` в векторе обычно работает за `O(1)`, а иногда за `O(n)`.

Кроме того, мы видим что перед самой первой итерацией и после выхода из цикла
`KeepRunning` вызывается какой-то [барьер](https://en.wikipedia.org/wiki/Barrier_(computer_science)).

Именно из-за этого барьера наш код тестирования HashTable работает корректно,
хоть объект хеш-таблицы создаётся и удаляется в одном из потоков.

```c++
void random_insertions(benchmark::State& state) {
  if (state.thread_index == 0) {
    TEST_TABLE.reset(new ConcurrentHashMap<int, int>(undefined_size, state.threads));
  }

  Random random(SEED + state.thread_index * 10);
  while (state.KeepRunning()) {
    // безопасно пользоваться таблицей, потому что вход в цикл - это барьер
    TEST_TABLE->insert(random(), 1); 
  }

  if (state.thread_index == 0) {
    // таблицу безопасно удалять, потому что выход из цикла - это барьер для всех потоков
    TEST_TABLE.reset(); 
  }
}
```

## Измерение времени

Все что нам осталось - это узнать как реализованы функции `ResumeTiming` и `StartTiming`.
  - Найдите эти методы в коде. Скорее всего они находятся в файле
    `benchmark.cc`, поэтому вам не нужно использовать `ack-grep`. Достаточно
    будет обычного поиска в редакторе.

Оказывается что функци делают пару проверок и зовут `StartTimer()` и `StopTimer()`.
  - Найдите эти методы в коде.

```c++
void StartTimer() {
  running_ = true;
  start_real_time_ = ChronoClockNow();
  start_cpu_time_ = ThreadCPUUsage();
}

// Called by each thread                                                                                                                                               
void StopTimer() {
  CHECK(running_);
  running_ = false;
  real_time_used_ += ChronoClockNow() - start_real_time_;
  cpu_time_used_ += ThreadCPUUsage() - start_cpu_time_;
}
```

`StartTimer()` запоминает текущее время на часах, а `StopTimer()` прибавляет
разность между значениями к счётчику.
  - Найдите реализации функций `ChronoClockNow()` и `ThreadCPUUsage()`. Тут вам уже потребуется `ack-grep`.

  1. Метод `ThreadCPUUsage()` реализован по разному на разных платформах. В нашем
     случае он использует `clock_gettime(CLOCK_THREAD_CPUTIME_ID)`.

  2. Видно, что `ChronoClockNow()` - это небольной wrapper над
     `std::chrono::high_resolution_clock`, который в свою очередь является
     typedef-ом на `std::chrono::system_clock`.

Разберёмся до конца, как работают наши часы. Нам потребуется gdb.
```
$ gdb ./build/release/bench_hash_map
(gdb) b std::chrono::_V2::steady_clock::now()
(gdb) run
Breakpoint 2, 0x00007ffff78e9480 in std::chrono::_V2::steady_clock::now() () from /usr/lib/x86_64-linux-gnu/libstdc++.so.6
(gdb) bt
#0  0x00007ffff78e9480 in std::chrono::_V2::steady_clock::now() () from /usr/lib/x86_64-linux-gnu/libstdc++.so.6
#1  0x0000000000428836 in benchmark::State::StartKeepRunning() ()
#2  0x0000000000411590 in random_insertions(benchmark::State&) ()
```

Первый вызов `StartKeepRunning()` в тесте random_insertions. 
Обратите внимание, что в стеке нет вызова `KeepRunning()` - компилятор заинлайнил метод целиком.
Исходных кодов от libstdc++ у нас нет, но это не беда.

```
(gdb) disassemble
Dump of assembler code for function _ZNSt6chrono3_V212steady_clock3nowEv:
=> 0x00007ffff78e9480 <+0>:	sub    $0x28,%rsp
   0x00007ffff78e9484 <+4>:	mov    $0x1,%edi
   0x00007ffff78e9489 <+9>:	mov    %rsp,%rsi
   0x00007ffff78e948c <+12>:	mov    %fs:0x28,%rax
   0x00007ffff78e9495 <+21>:	mov    %rax,0x18(%rsp)
   0x00007ffff78e949a <+26>:	xor    %eax,%eax
   0x00007ffff78e949c <+28>:	callq  0x7ffff78bf080 <clock_gettime@plt>
   0x00007ffff78e94a1 <+33>:	imul   $0x3b9aca00,(%rsp),%rax
   0x00007ffff78e94a9 <+41>:	add    0x8(%rsp),%rax
   0x00007ffff78e94ae <+46>:	mov    0x18(%rsp),%rdx
   0x00007ffff78e94b3 <+51>:	xor    %fs:0x28,%rdx
   0x00007ffff78e94bc <+60>:	jne    0x7ffff78e94c3 <_ZNSt6chrono3_V212steady_clock3nowEv+67>
   0x00007ffff78e94be <+62>:	add    $0x28,%rsp
   0x00007ffff78e94c2 <+66>:	retq   
   0x00007ffff78e94c3 <+67>:	callq  0x7ffff78c0bf0 <__stack_chk_fail@plt>
```

В `0x00007ffff78e949c <+28>` происходит вызов clock_gettime. Опять?

```
(gdb) b clock_gettime
Breakpoint 3 at 0x7ffff7064c60: clock_gettime. (2 locations)
(gdb) c
Continuing.

Breakpoint 3, __GI___clock_gettime (clock_id=1, tp=0x7fffffffc0d0) at ../sysdeps/unix/clock_gettime.c:93
93	../sysdeps/unix/clock_gettime.c: No such file or directory.
(gdb) c
Continuing.

Breakpoint 3, 0x00007ffff7ffaab4 in clock_gettime ()
(gdb) c
Continuing.

Breakpoint 3, __GI___clock_gettime (clock_id=3, tp=0x7fffffffc0d0) at ../sysdeps/unix/clock_gettime.c:93
93	in ../sysdeps/unix/clock_gettime.c
(gdb) c
Continuing.

Breakpoint 3, 0x00007ffff7ffaab4 in clock_gettime ()
(gdb) 
```

Видим, что `clock_gettime` вызывается с параметром `clock_id=1` и `clock_id=3`.
Один из этих вызовов должен получать `RealTime` а другой `CPUTime`.

Чтобы пройти дальше нужно ковыряться в внутренностях glibc и linux. 

А чтобы узнать RealTime на архитектуре x86 достаточно прочитать пару значений
из памяти `vdso` и выполнить инструкцию [`rdtsc`](https://ru.wikipedia.org/wiki/Rdtsc).
[Реализация clock_gettime для CLOCK_REALTIME](http://lxr.free-electrons.com/source/arch/x86/vdso/vclock_gettime.c?v=3.14#L173)
Смотрите функции `do_realtime` и `vgetns`.

Чтобы узнать `CPUTime` вам всегда нужно сделать системный вызов. В конечном
счёте `CPUTime` подсчитывается планировщиком. Он использует это значение чтобы
обеспечивать честное разделение времени между потоками.

В случае `google-benchmark` накладные расходы на получение времени не играют
особого значения, потому что измерения происходят только в начале и конце всего
теста. Однако из-за этого библиотека не может поймать тонкие моменты, когда
операция почти всегда работает быстро но иногда замедляется (такие как
реаллокация вектора или рехеширование хеш-таблицы).
