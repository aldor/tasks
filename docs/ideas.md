# Семинар 1. Потоки и mutex-ы.

## Задачи
 1. Параллельный архиватор
 2. Реализуем аналог std::async (join() / detach())
 3. Пишем std::shared_mutex, через std::mutex
 4. Найти и починить deadlock
 5. Задачку на spinlock. Что это такое, когда может быть нужно
 6. Посмотреть кто голодает в shared_mutex.

## Tutorial
 1. Реализация mutex
 2. Патчить ethereum CPUMiner
 3. Locking внутри malloc

# Семинар 2. condition_variable, барьеры, семафоры
