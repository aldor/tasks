# Инструкция по настройке VirtualBox

 1. Скачайте и установите virtualbox с [официального сайта](https://www.virtualbox.org/wiki/Downloads).
 2. Скачайте **64-битный** образ Ubuntu 16.04 с сайта http://www.osboxes.org/ubuntu/
 3. Импортируйте образ внутрь VirtualBox.
    - Создать
    - Тип операционной системы - Линукс Ubuntu 64bit
    - Объем оперативной памяти: 2GB
    - Использовать существующий виртуальный диск (тут указываем `.vdi` с образом, из 7z архива который вы скачали)
 4. Загружаем получившуюся виртуальную машину. (пароль `osboxes.org`).
 5. Открываем терминал и устанавлеваем Guest Additions. Они позволят интегрировать desktop гостевой OS и вашей 
    родной. Можно будет свободно перетаскивать файлы, шарить папку между двумя системами и не спотыкаться об захват курсора.

    `sudo sed -i 's/gb[.]/ru[.]/g' /etc/apt/sources.list`
    
    `sudo apt update && sudo apt install virtualbox-guest-dkms virtualbox-guest-x11 virtualbox-guest-utils`
    
 6. Выключаем машину и заходим в настройки машины (Меню по клику правой кнопкой мыши по машине в списке).
    - `Общие -> Дополнительно`
      Включаем тут общий буффер обмена и функцию Drag'n'Drop.
    - `Общие папки`
      Надо добавить сюда какую-нибудь папку, в которой вам будет удобно хранить код.
      Поставьте галочку "автоподключение".
    - `Система -> Процессор`
      Крутим ползунок чтобы отдать виртуалке все ядра (но не больше, чем есть на самом деле).
 7. Запускаем виртуалку снова.
   - Проверяем что правильно настроился Copy-Paste. _Как потом будете код копипастить со stackoverflow без этого?_
   - Проверяем что правильно настроился cpu. `cat /proc/cpuinfo` в терминале.
   - Проверяем что появилась shared папка. `ls /media`.
   - Добавляем пользователя в группу имеющую доступ к shared папке и перелогиниваемся. `sudo usermod osboxes -a -G vboxsf`.