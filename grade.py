#!/usr/bin/python3
"""Automation tool for c++ course in SHAD.

Usage:
  grade.py pre-release-check [--task=<task-name>] [--dry-run]
  grade.py grade --task=<task-name> --project=<project-name>
                 [--submit-root=<path>] [--rejudge] [--dry-run]
  grade.py start-rejudge --task=<task-name>
  grade.py status
  grade.py clean [--dry-run]
  grade.py gitlab (add-remotes|check-token)
  grade.py gdoc (sync-tables|check-token)

Options:
  --task=<task-name>
  --submit-root=<path>      Path to root of student repo.
  --dry-run                 Don't execute any commads, just print when to stdout. [default: False]

"""
import yaml
import os
import sys
import glob
import pytz
import datetime
import pprint
import itertools
import json
import collections
import subprocess
import re
import logging
import docopt
import gspread
import gitlab
import requests
import colored
import codecs
import pwd
import grp
import resource

from colored import stylize
from oauth2client.service_account import ServiceAccountCredentials

logger = logging.getLogger(__name__)


TZ_MOSCOW = pytz.timezone('Europe/Moscow')
TIME_PATTERN = "%d-%m-%Y %H:%M"
SPREADSHEET_ID = "1DFkY0LygOIP-pFY4HCJiWqPOQBSJSS60mkRpqdNW8Wk"


def print_info(*args):
    print(*args, file=sys.stderr)
    sys.stderr.flush()


class PipelineBuilder:
    def __init__(self):
        self.cmds = []
        self.ctx = {}

    def run(self, cmd, **kwargs):
        self.cmds.append((cmd, kwargs))

    def run_in_sandbox(self, cmd, limit_memory=True, **kwargs):
        ENV_WHITELIST = ["PATH"]
        MEMORY_LIMIT = 1024 ** 3 # 1 GB
        TIMEOUT_SECONDS = 20
        NUM_CPUS = 4

        def set_up_sandbox():
            if limit_memory:
                resource.setrlimit(resource.RLIMIT_AS,
                                   (MEMORY_LIMIT, MEMORY_LIMIT))
            else:
                print("WARNING: Not limiting memory usage", file=sys.stderr)

                uid = pwd.getpwnam("nobody").pw_uid
            gid = grp.getgrnam("nogroup").gr_gid
            try:
                os.setgroups([])
                os.setresgid(gid, gid, gid)
                os.setresuid(uid, uid, uid)
            except:
                print("WARNING: UID and GID change failed, running with current user",
                      file=sys.stderr)

            env = os.environ.copy()
            os.environ.clear()
            for variable in ENV_WHITELIST:
                os.environ[variable] = env[variable]

        self.run(cmd, preexec_fn=set_up_sandbox, timeout=TIMEOUT_SECONDS, **kwargs)

    def execute(self, dry_run=False):
        for cmd, kwargs in self.cmds:
            if isinstance(cmd, list):
                redirect_hook = None
                report = None
                if "redirect_hook" in kwargs:
                    redirect_hook = kwargs["redirect_hook"]
                    del kwargs["redirect_hook"]

                    cmd, report = redirect_hook(cmd, dry_run)

                cmdline = " ".join(cmd)

                if "preexec_fn" in kwargs:
                    cmdline = "sandbox " + cmdline

                if "cwd" in kwargs:
                    cmdline = "cd {} && ".format(kwargs["cwd"]) + cmdline
                print_info(">", cmdline)

                if dry_run:
                    continue

                subprocess.run(cmd, close_fds=False, check=True, **kwargs)
            elif callable(cmd):
                args=", ".join("{}={}".format(k, repr(v)) for k, v in sorted(kwargs.items()))
                print_info("$ {}({})".format(cmd.__name__, args))

                if dry_run:
                    continue
                
                cmd(self.ctx, **kwargs)

def check_regexp(ctx, filename, regexps):
    file_content = codecs.open(filename, encoding='utf-8').read()
    for regexp in regexps:
        if re.search(regexp, file_content, re.MULTILINE):
            raise ValueError("File {} contains banned regexp '{}'".format(
                filename, regexp))

def compare_benchmarks(ctx, baseline, solution):
    baseline = json.load(open(baseline))["benchmarks"]
    solution = json.load(open(solution))["benchmarks"]

    perf_score = 0
    for result, reference in zip(solution, baseline):
        perf_score += float(reference["cpu_time"] + 1) / (result["cpu_time"] + 1)
        perf_score += float(reference["real_time"] + 1) / (result["real_time"] + 1)

    if len(baseline) > 0:
        perf_score /= 2 * len(baseline)

    if 0.9 < perf_score < 1.0:
        perf_score = 1.0

    ctx["perf_score"] = perf_score


def find_student_row(worksheet, student_name):
    all_students = worksheet.range(3, 3, worksheet.row_count,3)

    # Gitlab converts names to lowercase and replaces '.' with '-'
    student_name = student_name.lower().replace('.', '-')
    for student_row in range(len(all_students)):
        sheet_name = all_students[student_row].value.lower().replace('.', '-')
        if sheet_name == student_name:
            return student_row + 3
    raise ValueError("Login {} not found in spreadsheet".format(login))


def gtofloat(s):
    if s:
        return float(s.replace(",", "."))
    else:
        return 0.0

def floattog(f):
    return "{:.2}".format(f).replace(".", ",")


def put_score_in_gdoc(ctx, sheet, task, student_name, rejudge):
    now = datetime.datetime.now(TZ_MOSCOW)
    task_num = int(task.name[:2])
    score = task.max_score
    
    if "perf_score" in ctx:
        print_info(">> Performance score: {:.2}".format(ctx["perf_score"]))
        score = score * 0.7 + ctx["perf_score"] * 0.3
    
    print_info(">> Total score: {:.2}".format(score))
    if now > task.deadline and not rejudge:
        score *= 0.3
        print_info(">> Applying deadline penalty, final score: {:.2}".format(score))

    if not rejudge:
        scores = sheet.worksheet("Оценки")
        student_row = find_student_row(scores, student_name)

        prev_score = scores.cell(student_row, 3 + task_num)
        if gtofloat(prev_score.value) < score:
            prev_score.value = floattog(score)
            scores.update_cells([prev_score])
    else:
        rejudge = sheet.worksheet("Rejudge")
        student_row = find_student_row(rejudge, student_name)
        rejudge.update_cell(student_row, 3 + task_num, floattog(score))    

    if "perf_score" in ctx:
        leaderboard = sheet.worksheet("Leaderboard")
        student_row = find_student_row(leaderboard, student_name)
        leaderboard.update_cell(student_row, 3 + task_num, floattog(ctx["perf_score"]))

    
class Task:
    def __init__(self, name, deadline, config):
        self.name = name
        self.deadline = deadline
        self._config = config

        self.sources = config.get("allow_change", [])
        if not self.sources:
            raise ValueError("No sources specified in 'allow_change' for %s" % name)

        for src in self.sources:
            if not os.path.exists(os.path.join(name, src)):
                raise ValueError("Source file %s not found in %s" % (src, name))

        self.forbidden_regexp = config.get("forbidden_regexp", [])
        # Cheap and dirty way to protect against
        # #include <opt/task_name/solution.cpp>
        self.forbidden_regexp += [".*{}.*".format(self.name)]
        for regexp in self.forbidden_regexp:
            re.compile(regexp)

        self.sanitizers = ["asan", "tsan"]
        if "disable_sanitizers" in config and config["disable_sanitizers"]:
            self.sanitizers = []

        self.gtests = config.get("gtests", [])
        if not isinstance(self.gtests, list):
            self.gtests = [self.gtests]
        
        self.benchmarks = config.get("benchmarks")

        self.solutions = config.get("solutions", [])
        self.compare_to = config.get("compare_to")
        if (self.compare_to is not None) and (self.compare_to not in self.solutions):
            raise ValueError("Can't compare to {}. Solution not found.".format(self.compare_to))

        self.private_tests = config.get("private_tests", False)

        self.max_score = float(config.get("max_score", 1.0))
        self.review = config.get("review", False)

    def __repr__(self):
        return "Task(name='{}')".format(self.name)
        
    def build_name(self, build_type, solution=None):
        build_name = build_type
        if solution is not None:
            build_name += "_" + solution
        return build_name

    def benchmark_json(self, solution=None):
        return os.path.join(self.name, "build", self.build_name("release", solution),
                            self.benchmarks + ".json")
    
    def gen_build(self, p, build_type, solution=None, with_private_tests=True):
        build_name = self.build_name(build_type, solution)
        build_dir = os.path.join(self.name, "build", build_name)
            
        p.run(["mkdir", "-p", build_dir])
        p.run(["cmake", "../../", "-G", "Ninja",
               "-DCMAKE_BUILD_TYPE=" + build_type,
               "-DENABLE_PRIVATE_TESTS=1"] +
              ([] if solution is None else ["-DTEST_SOLUTION=" + solution]),
              cwd=build_dir)
        p.run(["ninja", "-v", "-C", build_dir])

    def gen_gtests(self, p, build_type, test, solution=None,
                   sandbox=False, sandbox_memory_limit=True):
        build_name = self.build_name(build_type, solution)
        cmd = ["./{}".format(test)]
        kwargs = {"cwd": os.path.join(self.name, "build", build_name)}

        if sandbox:
            p.run_in_sandbox(cmd, sandbox_memory_limit, **kwargs)
        else:
            p.run(cmd, **kwargs)

    def gen_benchmarks(self, p, build_type, solution=None,
                       sandbox=False):
        build_name = self.build_name(build_type, solution)
        benchmark_json = self.benchmark_json(solution)

        cmd = ["./{}".format(self.benchmarks)]
        kwargs = {"cwd": os.path.join(self.name, "build", build_name)}

        def redirect_report(cmd, dry_run):
            if not dry_run:
                report = open(benchmark_json, "w")
                os.set_inheritable(report.fileno(), True)
                return cmd + ["--benchmark_out_format=json",
                              "--benchmark_out=/proc/self/fd/" + str(report.fileno())], report
            else:
                return cmd + ["--benchmark_out_format=json",
                              "--benchmark_out=/proc/self/fd/1234"], None

        if sandbox:
            p.run_in_sandbox(cmd, redirect_hook=redirect_report, **kwargs)
        else:
            p.run(cmd, redirect_hook=redirect_report, **kwargs)

    def copy_sources(self, p, submit_root):
        for src in self.sources:
            p.run(["cp", os.path.join(submit_root, self.name, src),
                   os.path.join(self.name, src)])

            if self.forbidden_regexp:
                p.run(check_regexp, filename=os.path.join(self.name, src),
                      regexps=self.forbidden_regexp)
        
    def pre_release_check(self, p):
        p.run(["mkdir", "-p", os.path.join(self.name, "build")])

        for solution in self.solutions:
            self.gen_build(p, "release", solution)

            for san in self.sanitizers:
                self.gen_build(p, san, solution)

            for test in self.gtests:
                self.gen_gtests(p, "release", test, solution)
                for san in self.sanitizers:
                    self.gen_gtests(p, san, test, solution)

            if self.benchmarks is not None:
                self.gen_benchmarks(p, "release", solution)

    def grade(self, submit_root=None):
        p.run(["mkdir", "-p", os.path.join(self.name, "build")])

        self.gen_build(p, "release")
        for san in self.sanitizers:
            self.gen_build(p, san)

        if self.compare_to is not None:
            self.gen_build(p, "release", self.compare_to)

        for test in self.gtests:
            self.gen_gtests(p, "release", test, sandbox=True)
            for san in self.sanitizers:
                self.gen_gtests(p, san, test, sandbox=True, sandbox_memory_limit=False)

        if self.benchmarks is not None:
            self.gen_benchmarks(p, "release", sandbox=True)
            if self.compare_to is not None:
                self.gen_benchmarks(p, "release", self.compare_to, sandbox=True)

            p.run(compare_benchmarks,
                  baseline=self.benchmark_json(self.compare_to),
                  solution=self.benchmark_json())            

    def clean(self, p):
        p.run(["rm", "-rf", os.path.join(self.name, "build")], check=False)

    def show_status(self):
        tags = []

        if self.gtests:
            tags.append(stylize("gtest", colored.fg("magenta") + colored.attr("bold")))
        else:
            tags.append(" " * 5)

        if self.benchmarks is not None:
            tags.append(stylize("bench", colored.fg("cyan")))
        else:
            tags.append(" " * 5)

        if self.sanitizers:
            for san in self.sanitizers:
                tags.append(stylize(san, colored.attr("bold") + colored.fg("green")))
        else:
            tags.append(" " * 9)    

        if self.private_tests:
            tags.append(stylize("private tests", colored.fg("blue")))
        else:
            tags.append(" " * len("private tests"))

        if self.compare_to:
            tags.append(stylize("perf score", colored.fg("purple_1b")))
        else:
            tags.append(" " * len("perf score"))

        if self.review:
            tags.append(stylize("review", colored.fg("red") + colored.attr("bold")))
        else:
            tags.append(" " * 6)

        score = stylize("{:3}".format(int(self.max_score)), colored.attr("bold"))
        print(" * {:30} {} - {}".format(self.name, score, " ".join(tags)))


class Course:
    def __init__(self, config):
        self._config = config

        self.tasks = collections.OrderedDict()
        self.groups = []
        
        for group in config:
            deadline = TZ_MOSCOW.localize(datetime.datetime.strptime(
                group["deadline"], TIME_PATTERN))
            group_name = group["name"]

            task_group = []
            for task_name in group.get("tasks", []):
                config = yaml.load(open(os.path.join(task_name, ".tester-config.yaml")))
                task = Task(task_name, deadline, config)
                task_group.append(task)

                self.tasks[task_name] = task


            self.groups.append({
                "name": group_name,
                "deadline": deadline,
                "tasks": task_group,
            })

    def clean(self, p):
        for task in self.tasks.values():
            task.clean(p)

    def pre_release_check(self, p, task=None):
        names = [task] if task is not None else self.tasks
        for task in names:
            self.tasks[task].pre_release_check(p)

    def grade(self, p, sheet, task_name, project, submit_root=None, rejudge=False):
        if task_name not in self.tasks:
            raise ValueError("Task %s not found" % task_name)

        task = self.tasks[task_name]
        if submit_root is not None and submit_root != '.':
            task.copy_sources(p, submit_root)

        task.grade(p)

        prefix = "student-"
        if not project.startswith(prefix):
            raise ValueError("Strange project name: {}", project)
        username = project[len(prefix):]

        if task.review:
            return
        
        p.run(put_score_in_gdoc, sheet=sheet,
              task=task, student_name=username, rejudge=rejudge)

    def show_status(self):
        for group in self.groups:
            deadline_passed = group["deadline"] < datetime.datetime.now(TZ_MOSCOW)
            
            print(stylize(group["deadline"].strftime(TIME_PATTERN),
                          colored.fg("yellow") if deadline_passed else colored.fg('green')),
                "-", stylize(group["name"], colored.attr("bold")))

            for task in group["tasks"]:
                task.show_status()


def get_sheet():
    ENV_ACCOUNT = "SHAD_GDOC_ACCOUNT"
    if ENV_ACCOUNT not in os.environ:
        raise ValueError(ENV_ACCOUNT + " is not set")

    scope = ['https://spreadsheets.google.com/feeds']    
    credentials = ServiceAccountCredentials.from_json_keyfile_dict(
        json.loads(os.environ["SHAD_GDOC_ACCOUNT"]), scope)
    
    gs = gspread.authorize(credentials)
    sheet = gs.open_by_key(SPREADSHEET_ID)
    return sheet


if __name__ == "__main__":
    args = docopt.docopt(__doc__, version='Grade Tool 2.0')

    course = Course(yaml.load(open(".deadlines.yml")))

    p = PipelineBuilder()
    
    if args["status"]:
        course.show_status()
    elif args["clean"]:
        course.clean(p)
        p.execute(dry_run=args["--dry-run"])
    elif args["pre-release-check"]:
        course.pre_release_check(p, args["--task"])
        p.execute(dry_run=args["--dry-run"])
    elif args["grade"]:
        sheet = None
        if args["--dry-run"] is not None:
            sheet = get_sheet()
        course.grade(p, sheet, args["--task"], args["--project"],
                     args["--submit-root"], args["--rejudge"])
        p.execute(dry_run=args["--dry-run"])
