#pragma once

#include <utility>

template<class T>
class BufferedChannel {
public:
    explicit BufferedChannel(int size) {}
    void send(const T& value) {}
    std::pair<T, bool> recv() {
        return std::make_pair(T(), false);
    }
    void close() {}
};
