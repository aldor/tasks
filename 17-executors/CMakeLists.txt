cmake_minimum_required(VERSION 2.8)
project(executors)

set(SRCS executors.cpp)
set(TEST_SRCS test.cpp)
set(BENCH_SRCS run.cpp)

if (TEST_SOLUTION)
  include_directories(../private/executors)
  set(SRCS ../private/executors/executors.cpp)
endif()

if (ENABLE_PRIVATE_TESTS)
  set(TEST_SRCS ${TEST_SRCS}
    ../private/executors/test_executors.cpp
    ../private/executors/test_future.cpp)
  set(BENCH_SRCS ${BENCH_SRCS} ../private/executors/run.cpp)
endif()

include(../common.cmake)

add_library(executors ${SRCS})

add_gtest(test_executors ${TEST_SRCS})
target_link_libraries(test_executors executors)

add_benchmark(bench_executors ${BENCH_SRCS})
target_link_libraries(bench_executors executors)
